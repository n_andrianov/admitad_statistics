package com.idt.rtm.admitad.statistics.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdmitadAction {
    String comment;
    String click_user_ip;
    String currency;
    String website_name;
    String status_updated;
    int id;
    int advcampaign_id;
    String subid1;
    String subid3;
    long subid2;
    String subid4;
    String click_user_referer;
    String click_date;
    int action_id;
    String status;
    String order_id;
    double cart;
    int conversion_time;
    int paid;
    double payment;
    String click_country_code;
    String advcampaign_name;
    int tariff_id;
    String keyword;
    String closing_date;
    AdmitadActionPosition[] positions;
    String subid;
    String action_date;
    int processed;
    String action_type;
    String action;
}
