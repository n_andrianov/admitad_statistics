package com.idt.rtm.admitad.statistics.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdmitadActionPosition {
    int tariff_id;
    String product_id;
    String product_name;
    String amount;
    String datetime;
    String product_url;
    double rate;
    String product_category_id;
    String product_category_name;
    int id;
    int rate_id;
    boolean percentage;
    double payment;
    String product_image;
}
