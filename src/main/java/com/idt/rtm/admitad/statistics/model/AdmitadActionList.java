package com.idt.rtm.admitad.statistics.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdmitadActionList {
    AdmitadAction[] results;
    AdmitadMeta _meta;
}
