package com.idt.rtm.admitad.statistics.mapper;

import org.apache.ibatis.annotations.Select;

public interface ClickhouseOffersClicksMapper {
    @Select("select op_type\n" +
            "from offers_clicks\n" +
            "where click_id=#{clickId}")
    String getOpType(long clickId);
}
