CREATE TABLE dcts.admitad_statistics (
    id NUMERIC PRIMARY KEY,
	action_id NUMERIC,
    order_id TEXT,
    subid1 TEXT,
    subid TEXT,

    website TEXT,
    advcampaign TEXT,
    action_name TEXT,

    order_sum TEXT,
    tariff TEXT,
    payment TEXT,
    partner_reward TEXT,
    user_reward TEXT,
    status TEXT,

    click_time TIMESTAMP WITHOUT TIME ZONE,
    click_type TEXT,
    action_time TIMESTAMP WITHOUT TIME ZONE,
    closing_date DATE,
    payment_time TIMESTAMP WITHOUT TIME ZONE,
    subid2 NUMERIC,

    product_name TEXT,
    product_image TEXT,
    product_url TEXT,
    country_code TEXT,
    category_id TEXT DEFAULT '-1',
    category_name TEXT
);

GRANT SELECT,INSERT,UPDATE ON dcts.admitad_statistics TO dcts;