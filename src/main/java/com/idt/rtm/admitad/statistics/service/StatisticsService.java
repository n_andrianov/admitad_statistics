package com.idt.rtm.admitad.statistics.service;

import com.idt.rtm.admitad.statistics.mapper.ClickhouseOffersClicksMapper;
import com.idt.rtm.admitad.statistics.mapper.StatisticsMapper;
import com.idt.rtm.admitad.statistics.model.*;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
@EnableScheduling
public class StatisticsService {

    @Autowired
    private StatisticsMapper statisticsMapper;

    @Autowired
    private ClickhouseOffersClicksMapper offersClicksMapper;

    @Value("${admitad.clientId}")
    private String admitadClientId;
    @Value("${admitad.base64Header}")
    private String admitadBase64Header;
    @Value("${cashback}")
    private double cashbackRatio;

    @Scheduled(cron = "${cron.statistics}")
    private void updateStatistics() throws IOException, ParseException {
        List<Statistics> statistics = getAdmitadStatistics();
        for (Statistics stat : statistics) {
            Statistics existingStatistics = statisticsMapper.getStatisticsById(stat.getId());
            if (existingStatistics == null) {
                statisticsMapper.insertStatistics(stat);
                continue;
            }
            if (!existingStatistics.getStatus().equals(stat.getStatus())) {
                statisticsMapper.updateStatistics(stat);
            }
        }
        statisticsMapper.flush();
    }

    private List<Statistics> getAdmitadStatistics() throws IOException, ParseException {
        List<Statistics> entries = new ArrayList<>();

        int offset = 0;
        int total = 1;
        int limit = 20;
        String accessToken = getAccessToken();
        BigDecimal currencyRate = getAdmitadCurrencyRate();

        while (offset < total) {
            AdmitadActionList actionList = getActions(accessToken, offset, limit);
            entries.addAll(convert(actionList, currencyRate));

            offset += limit;
            total = actionList.get_meta().getCount();
        }
        return entries;
    }

    private AdmitadActionList getActions(String accessToken, int offset, int limit) {
        String url = String.format("https://api.admitad.com/statistics/actions/?date_start=01.01.1970&offset=%d&limit=%d", offset, limit);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<AdmitadActionList> list = new RestTemplate(new SimpleClientHttpRequestFactory()).exchange(
                url,
                HttpMethod.GET,
                entity,
                AdmitadActionList.class);
        return list.getBody();
    }

    private String getAccessToken() throws IOException {
        String bodyData = "grant_type=client_credentials&" +
                "client_id=" + admitadClientId + "&" +
                "scope=advcampaigns arecords banners websites referrals advcampaigns_for_website public_data banners_for_website manage_advcampaigns statistics";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic " + admitadBase64Header);
        headers.set("charset", "utf-8");
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> entity = new HttpEntity<>(bodyData, headers);
        String url = "https://api.admitad.com/token/";
        ResponseEntity<AdmitadTokenResponse> tokenResponse = new RestTemplate(new SimpleClientHttpRequestFactory()).exchange(
                url,
                HttpMethod.POST,
                entity,
                AdmitadTokenResponse.class
        );
        if (200 <= tokenResponse.getStatusCodeValue() && tokenResponse.getStatusCodeValue() <= 299) {
            return tokenResponse.getBody().getAccessToken();
        } else {
            throw new IOException("Error with Admitad access token retrieval");
        }
    }

    private List<Statistics> convert(AdmitadActionList actionList, BigDecimal currencyRate) throws ParseException {
        List<Statistics> results = new ArrayList<>();

        for (AdmitadAction action : actionList.getResults()) {
            for (AdmitadActionPosition position : action.getPositions()) {

                Statistics statistics = new Statistics();
                statistics.setId(action.getId());
                statistics.setActionId(action.getTariff_id());
                statistics.setOrderId(action.getOrder_id());
                statistics.setSubid1(action.getSubid1());
                statistics.setSubid(action.getSubid());

                statistics.setWebsite(action.getWebsite_name());
                statistics.setAdvcampaign(action.getAdvcampaign_name());
                statistics.setActionName(action.getAction());

                statistics.setOrderSum(position.getAmount());
                statistics.setTariff(String.format("[%d,%d,%s%s]",
                        position.getTariff_id(), position.getRate_id(), String.valueOf(position.getRate()), position.isPercentage() ? "%" : ""));
                statistics.setPayment(String.format("%s %s", String.valueOf(position.getPayment()), action.getCurrency()));
                statistics.setStatus(action.getStatus());

                if (action.getStatus().equals("approved")) {
                    BigDecimal paymentInRub = action.getCurrency().equals("RUB")
                            ? BigDecimal.valueOf(position.getPayment())
                            : BigDecimal.valueOf(position.getPayment()).multiply(currencyRate)
                            .setScale(2, RoundingMode.HALF_EVEN);
                    BigDecimal userReward = paymentInRub.multiply(BigDecimal.valueOf(cashbackRatio))
                            .setScale(2, RoundingMode.HALF_EVEN);
                    statistics.setUserReward(userReward.toString());
                    statistics.setPartnerReward(paymentInRub.multiply(BigDecimal.valueOf((1 - cashbackRatio) / 2)).subtract(userReward)
                            .setScale(2, RoundingMode.HALF_EVEN).toString());
                }

                if (!StringUtils.isBlank(action.getClick_date()))
                    statistics.setClickTime(timestampFromString(action.getClick_date()));
                statistics.setClickType(offersClicksMapper.getOpType(action.getSubid2()));
                if (!StringUtils.isBlank(action.getAction_date()))
                    statistics.setActionTime(timestampFromString(action.getAction_date()));
                if (!StringUtils.isBlank(action.getClosing_date()))
                    statistics.setClosingDate(new Date(new SimpleDateFormat("yyyy-MM-dd").parse(action.getClosing_date()).getTime()));
                statistics.setPaymentTime(null);//TBD
                statistics.setSubid2(action.getSubid2());

                statistics.setProductName(position.getProduct_name());
                statistics.setProductImage(position.getProduct_image());
                statistics.setProductUrl(position.getProduct_url());
                statistics.setCountryCode(action.getClick_country_code());
                statistics.setCategoryId(StringUtils.isBlank(position.getProduct_category_id())
                        ? "-1" : position.getProduct_category_id());
                statistics.setCategoryName(position.getProduct_category_name());

                results.add(statistics);
            }
        }

        return results;
    }

    private BigDecimal getAdmitadCurrencyRate() throws IOException {

        String url = "https://api.admitad.com/currencies/rate/?base=USD&target=RUB&date=" + DateTime.now().toString("dd.MM.yyyy");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + getAccessToken());
        HttpEntity<?> entity = new HttpEntity<>(headers);

        RestTemplate rest = new RestTemplate();
        ResponseEntity<AdmitadCurrencyRate> rate = rest.exchange(
                url,
                HttpMethod.GET,
                entity,
                AdmitadCurrencyRate.class);

        if (rate.getStatusCodeValue() >= 200 && rate.getStatusCodeValue() < 299) {
            return new BigDecimal(rate.getBody().getRate()).setScale(2, RoundingMode.HALF_EVEN);
        } else {
            throw new IOException("Cannot get Currency Exchange Rate: "
                    + rate.getStatusCodeValue() + " Error:" + rate.getStatusCode().toString());
        }
    }

    private static Timestamp timestampFromString(String source) throws ParseException {
        return new Timestamp(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(source).getTime());
    }
}
