package com.idt.rtm.admitad.statistics.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "admitad_statistics")
public class Statistics {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "action_id")
    private int actionId;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "subid1")
    private String subid1;

    @Column(name = "subid")
    private String subid;

    @Column(name = "website")
    private String website;

    @Column(name = "advcampaign")
    private String advcampaign;

    @Column(name = "action_name")
    private String actionName;

    @Column(name = "order_sum")
    private String orderSum;

    @Column(name = "tariff")
    private String tariff;

    @Column(name = "payment")
    private String payment;

    @Column(name = "partner_reward")
    private String partnerReward;

    @Column(name = "user_reward")
    private String userReward;

    @Column(name = "status")
    private String status;

    @Column(name = "click_time")
    private Timestamp clickTime;

    @Column(name = "click_type")
    private String clickType;

    @Column(name = "action_time")
    private Timestamp actionTime;

    @Column(name = "closing_date")
    private Date closingDate;

    @Column(name = "payment_time")
    private Timestamp paymentTime;

    @Column(name = "subid2")
    private long subid2;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_image")
    private String productImage;

    @Column(name = "product_url")
    private String productUrl;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "category_id")
    private String categoryId;

    @Column(name = "category_name")
    private String categoryName;
}
