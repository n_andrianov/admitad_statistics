package com.idt.rtm.admitad.statistics.mapper;

import com.idt.rtm.admitad.statistics.model.Statistics;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface StatisticsMapper {

    @Select("SELECT *\n" +
            "FROM dcts.admitad_statistics")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "actionId", column = "action_id"),
            @Result(property = "orderId", column = "order_id"),
            @Result(property = "subid1", column = "subid1"),
            @Result(property = "subid", column = "subid"),
            @Result(property = "website", column = "website"),
            @Result(property = "advcampaign", column = "advcampaign"),
            @Result(property = "actionName", column = "action_name"),
            @Result(property = "orderSum", column = "order_sum"),
            @Result(property = "tariff", column = "tariff"),
            @Result(property = "payment", column = "payment"),
            @Result(property = "partnerReward", column = "partner_reward"),
            @Result(property = "userReward", column = "user_reward"),
            @Result(property = "status", column = "status"),
            @Result(property = "clickTime", column = "click_time"),
            @Result(property = "clickType", column = "click_type"),
            @Result(property = "actionTime", column = "action_time"),
            @Result(property = "closingDate", column = "closing_date"),
            @Result(property = "paymentTime", column = "payment_time"),
            @Result(property = "subid2", column = "subid2"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productImage", column = "product_image"),
            @Result(property = "productUrl", column = "product_url"),
            @Result(property = "countryCode", column = "country_code"),
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name")
    })
    List<Statistics> getAllStatistics();

    @Select("SELECT *\n" +
            "FROM dcts.admitad_statistics\n" +
            "WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "actionId", column = "action_id"),
            @Result(property = "orderId", column = "order_id"),
            @Result(property = "subid1", column = "subid1"),
            @Result(property = "subid", column = "subid"),
            @Result(property = "website", column = "website"),
            @Result(property = "advcampaign", column = "advcampaign"),
            @Result(property = "actionName", column = "action_name"),
            @Result(property = "orderSum", column = "order_sum"),
            @Result(property = "tariff", column = "tariff"),
            @Result(property = "payment", column = "payment"),
            @Result(property = "partnerReward", column = "partner_reward"),
            @Result(property = "userReward", column = "user_reward"),
            @Result(property = "status", column = "status"),
            @Result(property = "clickTime", column = "click_time"),
            @Result(property = "clickType", column = "click_type"),
            @Result(property = "actionTime", column = "action_time"),
            @Result(property = "closingDate", column = "closing_date"),
            @Result(property = "paymentTime", column = "payment_time"),
            @Result(property = "subid2", column = "subid2"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productImage", column = "product_image"),
            @Result(property = "productUrl", column = "product_url"),
            @Result(property = "countryCode", column = "country_code"),
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name")
    })
    Statistics getStatisticsById(int id);

    @Insert("INSERT INTO dcts.admitad_statistics(id, action_id, order_id, subid1, subid, website, advcampaign, action_name, " +
            "order_sum, tariff, payment, partner_reward, user_reward, status, click_time, click_type, action_time, " +
            "closing_date, payment_time, subid2, product_name, product_image, product_url, country_code, category_id, category_name)\n" +
            "VALUES(#{id}, #{actionId}, #{orderId}, #{subid1}, #{subid}, #{website}, #{advcampaign}, #{actionName}, " +
            "#{orderSum}, #{tariff}, #{payment}, #{partnerReward}, #{userReward}, #{status}, #{clickTime}, #{clickType}, " +
            "#{actionTime}, #{closingDate}, #{paymentTime}, #{subid2}, #{productName}, #{productImage}, #{productUrl}, " +
            "#{countryCode}, #{categoryId}, #{categoryName})")
    void insertStatistics(Statistics statistics);

    @Update("UPDATE dcts.admitad_statistics\n" +
            "SET id=#{id}, action_id=#{actionId}, order_id=#{orderId}, subid1=#{subid1}, subid=#{subid}, website=#{website}, " +
            "advcampaign=#{advcampaign}, action_name=#{actionName}, order_sum=#{orderSum}, tariff=#{tariff}, payment=#{payment}, " +
            "partner_reward=#{partnerReward}, user_reward=#{userReward}, status=#{status}, click_time=#{clickTime}, " +
            "click_type=#{clickType}, action_time=#{action_time}, closing_date=#{closingDate}, payment_time=#{paymentTime}, " +
            "subid2=#{subid2}, product_name=#{productName}, product_image=#{productImage}, product_url=#{productUrl}, " +
            "country_code=#{countryCode}, category_id=#{categoryId}, category_name=#{categoryName}\n" +
            "WHERE id=#{id}")
    void updateStatistics(Statistics statistics);

    @Flush
    void flush();
}
