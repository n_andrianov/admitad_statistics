package com.idt.rtm.admitad.statistics.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdmitadCurrencyRate {
    String date;
    String rate;
    String base;
    String target;
}
