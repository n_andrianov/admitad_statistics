package com.idt.rtm.admitad.statistics.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdmitadTokenResponse {
    String username;
    @JsonProperty("first_name")
    String firstName;
    @JsonProperty("last_name")
    String lastName;
    String language;
    @JsonProperty("access_token")
    String accessToken;
    @JsonProperty("expires_in")
    long expires;
    @JsonProperty("token_type")
    String tokenType;
    String scope;
    long id;
    @JsonProperty("refresh_token")
    String refreshToken;
}
