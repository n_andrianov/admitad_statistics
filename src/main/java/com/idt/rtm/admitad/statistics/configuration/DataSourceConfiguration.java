package com.idt.rtm.admitad.statistics.configuration;

import com.idt.rtm.admitad.statistics.mapper.ClickhouseOffersClicksMapper;
import com.idt.rtm.admitad.statistics.mapper.StatisticsMapper;
import com.idt.rtm.admitad.statistics.model.Statistics;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.ExecutorType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "com.idt.rtm.admitad.statistics")
public class DataSourceConfiguration {

    @Primary
    @Bean
    public DataSource postgresDataSource(
            @Value("${postgresql.user}") String user,
            @Value("${postgresql.password}") String password,
            @Value("${postgresql.hostname}") String hostname,
            @Value("${postgresql.dbname}") String dbname,
            @Value("${postgresql.ssl}") String ssl,
            @Value("${postgresql.sslFactory}") String sslfactory
    ) {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName(org.postgresql.ds.PGSimpleDataSource.class.getName());
        config.setUsername(user);
        config.setPassword(password);
        config.addDataSourceProperty("databaseName", dbname);
        config.addDataSourceProperty("serverName", hostname);
        config.addDataSourceProperty("ssl", ssl);
        config.addDataSourceProperty("sslfactory", sslfactory);
        return new HikariDataSource(config);
    }

    @Bean
    public DataSourceTransactionManager transactionManager(
            @Qualifier("postgresDataSource") @Autowired DataSource ds
    ) {
        return new DataSourceTransactionManager(ds);
    }

    private SqlSessionFactoryBean sqlSessionFactory(
            @Qualifier("postgresDataSource") @Autowired DataSource ds
    ) {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(ds);
        sqlSessionFactory.setTypeAliases(new Class[]{
                Statistics.class
        });
        return sqlSessionFactory;
    }

    @Bean
    public SqlSessionTemplate postgresSqlSessionTemplate(
            @Qualifier("postgresDataSource") DataSource ds
    ) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory(ds).getObject());
    }

    @Bean
    public MapperFactoryBean<StatisticsMapper> statisticsMapper(
            @Qualifier("postgresSqlSessionTemplate") SqlSessionTemplate sqlSessionTemplate
    ) {
        MapperFactoryBean<StatisticsMapper> mapper = new MapperFactoryBean<>(StatisticsMapper.class);
        mapper.setSqlSessionTemplate(sqlSessionTemplate);
        return mapper;
    }

    @Bean
    public DataSource clickhouseDataSource(
            @Value("${clickhouse.url}") String clickhouseUrl,
            @Value("${clickhouse.database}") String clickhouseDatabase,
            @Value("${clickhouse.user}") String clickhouseUser,
            @Value("${clickhouse.password}") String clickhousePassword
    ) {
        ClickHouseProperties properties = new ClickHouseProperties();
        properties.setDatabase(clickhouseDatabase);
        properties.setUser(clickhouseUser);
        properties.setPassword(clickhousePassword);
        properties.setUseServerTimeZone(false);
        properties.setUseServerTimeZoneForDates(false);
        properties.setUseTimeZone("UTC");
        return new ClickHouseDataSource(clickhouseUrl, properties);
    }

    @Bean
    public DataSourceTransactionManager clickhouseTransactionManager(
            @Qualifier("clickhouseDataSource") DataSource ds
    ) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(ds);
        return transactionManager;
    }

    private SqlSessionFactoryBean clickhouseSqlSessionFactory(
            @Qualifier("clickhouseDataSource") DataSource ds
    ) {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(ds);
        return sqlSessionFactory;
    }

    @Bean
    public SqlSessionTemplate batchSqlSessionTemplate(
            @Qualifier("clickhouseDataSource") DataSource ds
    ) throws Exception {
        return new SqlSessionTemplate(clickhouseSqlSessionFactory(ds).getObject(), ExecutorType.BATCH);
    }


    @Bean
    public MapperFactoryBean<ClickhouseOffersClicksMapper> clickhouseOffersClicksMapper(
            @Qualifier("batchSqlSessionTemplate") SqlSessionTemplate batchSqlSessionTemplate
    ) {
        MapperFactoryBean<ClickhouseOffersClicksMapper> mapper = new MapperFactoryBean<>(ClickhouseOffersClicksMapper.class);
        mapper.setSqlSessionTemplate(batchSqlSessionTemplate);
        return mapper;
    }
}
